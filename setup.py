from setuptools import setup, find_packages


setup(
    name='curd',
    version='0.0.13',
    url='https://gitlab.com/hmx19/curd',
    description='db operations',
    author='',
    author_email='',
    license='MIT',
    keywords='db operations',
    packages=find_packages(),
    install_requires=[],
    extras_require={
        'cassandra': ['cassandra-driver==3.11.0'],
        'mysql': ['PyMySQL==0.7.11'],
        'clickhouse-driver': ['clickhouse-driver==0.2.6']
    }
)
