# database
cassandra-driver==3.11.0
pymongo==3.5.1
PyMySQL==0.7.11
phoenixdb==0.7
clickhouse-driver==0.2.6
# test
pytest
