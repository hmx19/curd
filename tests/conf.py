mysql_conf = {
    'type': 'mysql',
    'conf': {
        'host': '10.125.0.28',
        'port': 3306,
        'user': 'speedy',
        'password': 'soc-ti-mysql',
        'db': 'speedy'
    }
}

cassandra_conf = {
    'type': 'cassandra',
    'conf': {
        'hosts': ['127.0.0.1'],
    }
}

hbase_conf = {
    'type': 'hbase',
    'conf': {
        'urls': ['http://127.0.0.1:8765/'],
    }
}

postgresql_conf = {
    'type': 'postgresql',
    'conf': {
        'host': '127.0.0.1',
        'port': 5432,
        'user': 'postgres',
        'password': 'root',
        'database': 'db_crawloop_engine'
    }
}

clickhouse_conf = {
    'type': 'clickhouse',
    'conf': {
        'url': 'clickhouse://127.0.0.1:9000'
    }
}

