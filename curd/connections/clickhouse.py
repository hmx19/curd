import copy
from clickhouse_driver import connect

from . import logger
from ..errors import (
    UnexpectedError, OperationFailure, ProgrammingError,
    ConnectError,
    DuplicateKeyError
)
from .utils.sql import (
    query_parameters_from_create,
    query_parameters_from_update,
    query_parameters_from_delete,
    query_parameters_from_filter
)
from . import (
    BaseConnection, DEFAULT_FILTER_LIMIT, DEFAULT_TIMEOUT, OP_RETRY_WARNING,
    CURD_FUNCTIONS
)
from .mysql import MysqlConnection, MysqlConnectionPool


class ClickHouseConnection(MysqlConnection):

    def __init__(self, conf):
        MysqlConnection.__init__(self, conf)

    def _connect(self, conf):
        conf = copy.deepcopy(conf)

        self.max_op_fail_retry = conf.pop('max_op_fail_retry', 0)
        self.default_timeout = conf.pop('timeout', DEFAULT_TIMEOUT)

        conn = connect(conf['url'])
        cursor = conn.cursor()
        return conn, cursor

    def _execute(self, query, params, timeout, cursor_func='execute'):

        if not self.cursor:
            self.connect(self._conf)

        self.conn._read_timeout = timeout
        self.conn._write_timeout = timeout

        try:
            self.cursor.execute(query, params)
        except Exception as e:
            raise OperationFailure(origin_error=e)
        else:
            return list(self.cursor.fetchall())

    def create(self, collection, data, mode='INSERT', compress_fields=None, **kwargs):
        query, params = query_parameters_from_create(
            collection, data, mode.upper(), compress_fields
        )
        query = self.adapt_standard_query(query, data)
        try:
            self.execute(query, data)
        except ProgrammingError as e:
            if e._origin_error.args[0] == self.pe_duplicate_entry_key_error_code:
                raise DuplicateKeyError(str(e._origin_error))
            else:
                raise

    def create_many(self, collection, data, mode='INSERT', compress_fields=None, **kwargs):
        raise NotImplementedError('not supported create_many')

    def update(self, collection, data, filters, **kwargs):
        raise NotImplementedError('not supported update')

    def delete(self, collection, filters, **kwargs):
        raise NotImplementedError('not supported delete')

    def filter(self, collection, filters=None, fields=None, order_by=None, limit=DEFAULT_FILTER_LIMIT, **kwargs):
        raise NotImplementedError('not supported filter')

    @staticmethod
    def adapt_standard_query(query, data):
        """ clickhouse sql do not support all standards, hack for quick implement
            1. '`' quote not support
            5. '%s' instead of '%(id)s'
        """
        fields = []
        for k, v in data.items():
            fields.append('%({})s'.format(k))
        new_query = query.replace('`', '') % tuple(fields)
        return new_query


class ClickConnectionPool(MysqlConnectionPool):
    def __init__(self, *args, **kwargs):
        MysqlConnectionPool.__init__(self, *args, **kwargs)

    def get_connection(self):
        return ClickHouseConnection(*self._args, **self._kwargs)
